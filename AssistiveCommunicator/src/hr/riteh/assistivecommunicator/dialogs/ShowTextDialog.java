package hr.riteh.assistivecommunicator.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import hr.riteh.assistivecommunicator.R;
import hr.riteh.assistivecommunicator.Settings;

public class ShowTextDialog extends AlertDialog implements DialogInterface.OnClickListener
{
	public ShowTextDialog(Context context)
	{
		super(context);
	}

	public ShowTextDialog(Context context, int theme)
	{
		super(context, theme);
	}

	public ShowTextDialog(Context context, boolean cancelable, OnCancelListener cancelListener)
	{
		super(context, cancelable, cancelListener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// Retrieve reference to context
		Context context = getContext();

		// Configure dialog
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		setButton(BUTTON_NEUTRAL, context.getText(R.string.close), this);

		// Let superclass do it's work
		super.onCreate(savedInstanceState);

		// Get access to application settings
		Settings settings = new Settings(context);

		// Change text size
		TextView textView = (TextView) findViewById(android.R.id.message);
		textView.setTextSize(settings.getShowTextSize());
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		// Do nothing
	}
}
