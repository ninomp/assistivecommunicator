package hr.riteh.assistivecommunicator.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import hr.riteh.assistivecommunicator.R;

public class SimpleMessageDialog extends AlertDialog implements DialogInterface.OnClickListener
{
	public SimpleMessageDialog(Context context)
	{
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// Get a reference to context
		Context context = getContext();

		// Configure dialog
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		setButton(BUTTON_NEUTRAL, context.getText(R.string.ok), this);

		// Call superclass to let it do it's work
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		// Do nothing
	}

	public void setMessage(int messageId)
	{
		setMessage(getContext().getText(messageId));
	}
}
