package hr.riteh.assistivecommunicator.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import hr.riteh.assistivecommunicator.R;

public class ConfirmDialog extends AlertDialog implements DialogInterface.OnClickListener
{
	private OnConfirmListener mListener;

	public ConfirmDialog(Context context)
	{
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		Context context = getContext();

		setCancelable(true);
		setCanceledOnTouchOutside(true);
		setButton(BUTTON_POSITIVE, context.getText(R.string.ok), this);
		setButton(BUTTON_NEGATIVE, context.getText(R.string.cancel), this);

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		if (which == BUTTON_POSITIVE && mListener != null)
		{
			mListener.onConfirm(this);
		}
	}

	public void setOnConfirmListener(OnConfirmListener listener)
	{
		mListener = listener;
	}

	public interface OnConfirmListener
	{
		public void onConfirm(DialogInterface dialog);
	}

	public void setMessage(int messageId)
	{
		super.setMessage(getContext().getString(messageId));
	}
}
