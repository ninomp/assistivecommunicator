package hr.riteh.assistivecommunicator;

import java.io.File;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

public class FileResourceManager
{
	// Directory names
	private static String DIRECTORY_IMAGES = "images";

	// Directories
	private File mImagesDir;

	public FileResourceManager(Context context)
	{
		// Initialize images directory
		mImagesDir = context.getDir(DIRECTORY_IMAGES, Context.MODE_PRIVATE);
	}

	public File getImagesDir()
	{
		return mImagesDir;
	}

	public File getImageFile(String imagefilename)
	{
		if (imagefilename == null)
		{
			return null;
		}
		else if (imagefilename.startsWith("/"))
		{
			return new File(imagefilename);
		}
		else
		{
			return new File(getImagesDir(), imagefilename);
		}
	}

	public String getImagePath(String imagefilename)
	{
		File imageFile = getImageFile(imagefilename);
		if (imageFile != null)
		{
			return imageFile.getAbsolutePath();
		}
		else
		{
			return null;
		}
	}

	public static String getFullPathFromUri(Context context, Uri contentUri)
	{
		String path = null;

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
		if (cursor != null)
		{
			cursor.moveToFirst();

			int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
			if (column_index >= 0)
			{
				path = cursor.getString(column_index);
			}

			cursor.close();
		}

		return path;
	}
}
