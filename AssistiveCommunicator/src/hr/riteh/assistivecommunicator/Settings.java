package hr.riteh.assistivecommunicator;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;

public class Settings
{
	public static final String SETTING_SHOWUSAGE = "showUsage";
	public static final String SETTING_ORIENTATIONMODE = "orientationMode";
	public static final String SETTING_CATEGORIESPAGESIZE = "categoriesPageSize";
	public static final String SETTING_SHOWTEXTSIZE = "showTextSize";
	public static final String SETTING_LISTTEXTSIZE = "listTextSize";
	public static final String SETTING_CATEGORIESORDER = "categoriesOrder";
	public static final String SETTING_SENTENCESORDER = "sentencesOrder";
	public static final String SETTING_SHOWTEXT = "showtext";
	public static final String SETTING_SPEAKTEXT = "speaktext";
	public static final String SETTING_KEYBOARDTYPE = "keyboardType";
	public static final String SETTING_WORDSUGGESTION = "wordSuggestion";

	private static final boolean DEFAULT_SHOWUSAGE = false;
	private static final String DEFAULT_ORIENTATIONMODE = "-1";
	private static final String DEFAULT_CATEGORIESPAGESIZE = "4";
	private static final String DEFAULT_SHOWTEXTSIZE = "30";
	private static final String DEFAULT_LISTTEXTSIZE = "15";
	private static final String DEFAULT_CATEGORIESORDER = "Usage DESC";
	private static final String DEFAULT_SENTENCESORDER = "Usage DESC";
	private static final boolean DEFAULT_SHOWTEXT = true;
	private static final boolean DEFAULT_SPEAKTEXT = true;
	private static final String DEFAULT_KEYBOARDTYPE = "qwerty";
	private static final String DEFAULT_WORDSUGGESTION = "3";

	private static final String VALUE_KEYBOARDTYPE_ALPHABETICAL = "alphabetical";
	private static final String VALUE_KEYBOARDTYPE_QWERTY = "qwerty";

	private SharedPreferences mPreferences;

	public Settings(Context context)
	{
		mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public Settings(SharedPreferences preferences)
	{
		mPreferences = preferences;
	}

	public void registerOnChangeListener(OnSharedPreferenceChangeListener listener)
	{
		mPreferences.registerOnSharedPreferenceChangeListener(listener);
	}

	public void unregisterOnChangeListener(OnSharedPreferenceChangeListener listener)
	{
		mPreferences.unregisterOnSharedPreferenceChangeListener(listener);
	}

	public boolean getShowUsage()
	{
		return mPreferences.getBoolean(SETTING_SHOWUSAGE, DEFAULT_SHOWUSAGE);
	}

	public int getOrientationMode()
	{
		return Integer.parseInt(mPreferences.getString(SETTING_ORIENTATIONMODE, DEFAULT_ORIENTATIONMODE));
	}

	public int getCategoriesPageSize()
	{
		return Integer.parseInt(mPreferences.getString(SETTING_CATEGORIESPAGESIZE, DEFAULT_CATEGORIESPAGESIZE));
	}

	public float getShowTextSize()
	{
		return Float.parseFloat(mPreferences.getString(SETTING_SHOWTEXTSIZE, DEFAULT_SHOWTEXTSIZE));
	}

	public float getListTextSize()
	{
		return Float.parseFloat(mPreferences.getString(SETTING_LISTTEXTSIZE, DEFAULT_LISTTEXTSIZE));
	}

	public String getCategoriesOrder()
	{
		return mPreferences.getString(SETTING_CATEGORIESORDER, DEFAULT_CATEGORIESORDER);
	}

	public String getSentencesOrder()
	{
		return mPreferences.getString(SETTING_SENTENCESORDER, DEFAULT_SENTENCESORDER);
	}

	public boolean getShowText()
	{
		return mPreferences.getBoolean(SETTING_SHOWTEXT, DEFAULT_SHOWTEXT);
	}

	public boolean getSpeakText()
	{
		return mPreferences.getBoolean(SETTING_SPEAKTEXT, DEFAULT_SPEAKTEXT);
	}

	public int getKeyboardType()
	{
		String type = mPreferences.getString(SETTING_KEYBOARDTYPE, DEFAULT_KEYBOARDTYPE);

		if (VALUE_KEYBOARDTYPE_ALPHABETICAL.equals(type))
		{
			return R.xml.keyboard_layout_alphabetical;
		}
		else if (VALUE_KEYBOARDTYPE_QWERTY.equals(type))
		{
			return R.xml.keyboard_layout_qwerty;
		}
		else
		{
			return R.xml.keyboard_layout_qwerty;
		}
	}

	public int getWordSuggestion()
	{
		return  Integer.parseInt(mPreferences.getString(SETTING_WORDSUGGESTION, DEFAULT_WORDSUGGESTION));
	}
}
