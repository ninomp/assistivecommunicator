package hr.riteh.assistivecommunicator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.text.TextUtils;

public abstract class CSV
{
	private static final String DELIMITER = ";";
	private static final String LINE_FEED = "\n";

	public static List<Map<String, String>> parseStream(InputStream stream, String encoding) throws IOException
	{
		CSV.MapReader reader = new CSV.MapReader(stream, encoding);

		List<Map<String, String>> data = reader.readAll();

		reader.close();

		return data;
	}

	public static class Reader
	{
		private InputStream mStream;
		private BufferedReader mReader;

		public Reader(InputStream inputStream)
		{
			mStream = inputStream;
			mReader = new BufferedReader(new InputStreamReader(inputStream));
		}

		public Reader(InputStream inputStream, String encoding) throws UnsupportedEncodingException
		{
			mStream = inputStream;
			mReader = new BufferedReader(new InputStreamReader(inputStream, encoding));
		}

		public String[] read() throws IOException
		{
			String line = mReader.readLine();
			if (line == null)  return null;
			return line.split(DELIMITER);
		}

		public void close() throws IOException
		{
			mReader.close();
			mStream.close();
		}
	}

	public static class MapReader
	{
		private Reader mReader;
		private String[] mHeaderRow;

		public MapReader(InputStream inputStream) throws IOException
		{
			mReader = new Reader(inputStream);
			mHeaderRow = mReader.read();
		}

		public MapReader(InputStream inputStream, String encoding) throws IOException
		{
			mReader = new Reader(inputStream, encoding);
			mHeaderRow = mReader.read();
		}

		public Map<String, String> read() throws IOException
		{
			String[] values = mReader.read();
			if (values == null)  return null;

			Map<String, String> map = new HashMap<String, String>(values.length);

			for (int i = 0; i < values.length; i++)
			{
				map.put(mHeaderRow[i], values[i]);
			}

			return map;
		}

		public List<Map<String, String>> readAll() throws IOException
		{
			List<Map<String, String>> all = new ArrayList<Map<String, String>>();

			Map<String, String> row;
			while ((row = read()) != null)
			{
				all.add(row);
			}

			return all;
		}

		public void close() throws IOException
		{
			mReader.close();
		}
	}

	public static class Writer
	{
		private StringBuilder mBuffer;

		public Writer()
		{
			mBuffer = new StringBuilder();
		}

		public void write(Object... values)
		{
			mBuffer.append(TextUtils.join(DELIMITER, values));
			mBuffer.append(LINE_FEED);
		}

		@Override
		public String toString()
		{
			return mBuffer.toString();
		}
	}
}
