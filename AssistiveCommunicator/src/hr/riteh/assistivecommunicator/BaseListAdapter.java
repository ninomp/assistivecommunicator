package hr.riteh.assistivecommunicator;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class BaseListAdapter<T> extends BaseAdapter
{
	List<T> mList;

	public BaseListAdapter(List<T> list)
	{
		mList = list;
	}

	public void setData(List<T> list)
	{
		mList = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		return mList.size();
	}

	@Override
	public T getItem(int position)
	{
		return mList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return getItemID(mList.get(position));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;

		if (view == null)
		{
			view = createView(parent);
		}

		bindView(view, mList.get(position));

		return view;
	}

	public abstract long getItemID(T item);
	public abstract View createView(ViewGroup parent);
	public abstract void bindView(View view, T item);
}
