package hr.riteh.assistivecommunicator;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database
{
	class OpenHelper extends SQLiteOpenHelper
	{
		// Database schema version
		private static final int DATABASE_VERSION = 1;

		// Database name
		private static final String DATABASE_NAME = "Communicator.db";

		// Table name constants
		private static final String TABLE_CATEGORY     = "Category";
		private static final String TABLE_SENTENCE     = "Sentence";
		private static final String TABLE_WORDSEQUENCE = "WordSequence";

		// Field name constants
		private static final String FIELDNAME_ID         = "ID";
		private static final String FIELDNAME_NAME       = "Name";
		private static final String FIELDNAME_CONTENT    = "Content";
		private static final String FIELDNAME_IMAGENAME  = "ImageName";
		private static final String FIELDNAME_USAGE      = "Usage";
		private static final String FIELDNAME_CATEGORYID = "CategoryID";
		private static final String FIELDNAME_THISWORD   = "ThisWord";
		private static final String FIELDNAME_NEXTWORD   = "NextWord";
		private static final String FIELDNAME_COUNTER    = "Count";

		// Field definition constants
		private static final String FIELD_ID         = FIELDNAME_ID         + " INTEGER NOT NULL PRIMARY KEY";
		private static final String FIELD_NAME       = FIELDNAME_NAME       + " TEXT NOT NULL";
		private static final String FIELD_CONTENT    = FIELDNAME_CONTENT    + " TEXT NOT NULL";
		private static final String FIELD_IMAGENAME  = FIELDNAME_IMAGENAME  + " TEXT";
		private static final String FIELD_USAGE      = FIELDNAME_USAGE      + " INTEGER NOT NULL DEFAULT 0";
		private static final String FIELD_CATEGORYID = FIELDNAME_CATEGORYID + " INTEGER NOT NULL";
		private static final String FIELD_AUTOINC_ID = FIELDNAME_ID         + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT";
		private static final String FIELD_THISWORD   = FIELDNAME_THISWORD   + " TEXT";
		private static final String FIELD_NEXTWORD   = FIELDNAME_NEXTWORD   + " TEXT";
		private static final String FIELD_COUNTER    = FIELDNAME_COUNTER    + " INTEGER NOT NULL DEFAULT 0";

		// SQL queries
		private static final String QUERY_CREATE_CATEGORY     = "CREATE TABLE " + TABLE_CATEGORY     + " (" + FIELD_ID + ", " + FIELD_NAME + ", " + FIELD_IMAGENAME + ", " + FIELD_USAGE + ")";
		private static final String QUERY_CREATE_SENTENCE     = "CREATE TABLE " + TABLE_SENTENCE     + " (" + FIELD_ID + ", " + FIELD_CATEGORYID + ", " + FIELD_CONTENT + ", " + FIELD_USAGE + ")";
		private static final String QUERY_CREATE_WORDSEQUENCE = "CREATE TABLE " + TABLE_WORDSEQUENCE + " (" + FIELD_AUTOINC_ID + ", " + FIELD_THISWORD + ", " + FIELD_NEXTWORD + ", " + FIELD_COUNTER + ")";

		public OpenHelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db)
		{
			db.execSQL(QUERY_CREATE_CATEGORY);
			db.execSQL(QUERY_CREATE_SENTENCE);
			db.execSQL(QUERY_CREATE_WORDSEQUENCE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
		}

		public void increment(String table, String fieldName, String whereFieldName, String whereFieldValue)
		{
			mDatabase.execSQL(String.format("UPDATE %s SET %s = %s + 1 WHERE %s = %s", table, fieldName, fieldName, whereFieldName, whereFieldValue));
		}
	}

	private OpenHelper mOpenHelper;
	private SQLiteDatabase mDatabase;

	public Database(Context context)
	{
		this(context, false);  // Open read-only database by default
	}

	public Database(Context context, boolean writable)
	{
		mOpenHelper = new OpenHelper(context);

		if (writable)
		{
			mDatabase = mOpenHelper.getWritableDatabase();
		}
		else
		{
			mDatabase = mOpenHelper.getReadableDatabase();
		}
	}

	public void close()
	{
		mDatabase.close();
	}

	public List<Category> getCategories(String orderBy)
	{
		List<Category> categories = new ArrayList<Category>();
		String fields [] = { OpenHelper.FIELDNAME_ID, OpenHelper.FIELDNAME_NAME, OpenHelper.FIELDNAME_IMAGENAME, OpenHelper.FIELDNAME_USAGE };
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_CATEGORY, fields, null, null, null, null, orderBy);
		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			categories.add(new Category(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
			cursor.moveToNext();
		}

		cursor.close();

		return categories;
	}

	public Category getCategory(long category_id)
	{
		Category category = null;
		String columns [] = { OpenHelper.FIELDNAME_ID, OpenHelper.FIELDNAME_NAME, OpenHelper.FIELDNAME_IMAGENAME, OpenHelper.FIELDNAME_USAGE };
		String selection = OpenHelper.FIELDNAME_ID + " = ?";
		String selectionArgs [] = { String.valueOf(category_id) };
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_CATEGORY, columns, selection, selectionArgs, null, null, null);

		if (cursor.moveToFirst())
		{
			category = new Category(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3));
		}

		cursor.close();

		return category;
	}

	public List<Sentence> getCategorySentences(long category_id, String orderBy)
	{
		List<Sentence> sentences = new ArrayList<Sentence>();
		String columns [] = { OpenHelper.FIELDNAME_ID, OpenHelper.FIELDNAME_CATEGORYID, OpenHelper.FIELDNAME_CONTENT, OpenHelper.FIELDNAME_USAGE };
		String selection_values [] = { String.valueOf(category_id) };
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_SENTENCE, columns, OpenHelper.FIELDNAME_CATEGORYID + " = ?", selection_values, null, null, orderBy);
		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			sentences.add(new Sentence(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getInt(3)));
			cursor.moveToNext();
		}

		cursor.close();

		return sentences;
	}

	public List<Sentence> getSentences(String orderBy)
	{
		List<Sentence> sentences = new ArrayList<Sentence>();
		String fields [] = { OpenHelper.FIELDNAME_ID, OpenHelper.FIELDNAME_CATEGORYID, OpenHelper.FIELDNAME_CONTENT, OpenHelper.FIELDNAME_USAGE };
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_SENTENCE, fields, null, null, null, null, orderBy);
		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			sentences.add(new Sentence(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getInt(3)));
			cursor.moveToNext();
		}

		cursor.close();

		return sentences;
	}

	public Sentence getSentence(long sentence_id)
	{
		Sentence sentence = null;
		String columns [] = { OpenHelper.FIELDNAME_ID, OpenHelper.FIELDNAME_CATEGORYID, OpenHelper.FIELDNAME_CONTENT, OpenHelper.FIELDNAME_USAGE };
		String selection = OpenHelper.FIELDNAME_ID + " = ?";
		String selectionArgs [] = { String.valueOf(sentence_id) };
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_SENTENCE, columns, selection, selectionArgs, null, null, null);

		if (cursor.moveToFirst())
		{
			sentence = new Sentence(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getInt(3));
		}

		cursor.close();

		return sentence;
	}

	public List<WordPairSequence> getWordPairSequences(String orderBy)
	{
		List<WordPairSequence> wordPairs = new ArrayList<WordPairSequence>();
		String fields [] = { OpenHelper.FIELDNAME_ID, OpenHelper.FIELDNAME_THISWORD, OpenHelper.FIELDNAME_NEXTWORD, OpenHelper.FIELDNAME_COUNTER };
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_WORDSEQUENCE, fields, null, null, null, null, orderBy);
		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			wordPairs.add(new WordPairSequence(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
			cursor.moveToNext();
		}

		cursor.close();

		return wordPairs;
	}

	public List<String> getWordSuggestions(String thisWord)
	{
		List<String> words = new ArrayList<String>();
		String fields [] = { OpenHelper.FIELDNAME_NEXTWORD };
		String selection = OpenHelper.FIELDNAME_THISWORD + " = ?";
		String selectionArgs [] = { thisWord };
		String orderBy = OpenHelper.FIELDNAME_COUNTER + " DESC";
		Cursor cursor = mDatabase.query(OpenHelper.TABLE_WORDSEQUENCE, fields, selection, selectionArgs, null, null, orderBy);
		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			words.add(cursor.getString(0));
			cursor.moveToNext();
		}

		cursor.close();

		return words;
	}

	public void putCategories(List<Category> categories)
	{
		for (Category category : categories)
		{
			putCategory(category);
		}
	}

	public void putSentences(List<Sentence> sentences)
	{
		for (Sentence sentence : sentences)
		{
			putSentence(sentence);
		}
	}

	public void putWordPairSequences(List<WordPairSequence> wordPairs)
	{
		for (WordPairSequence wordPair : wordPairs)
		{
			putWordPairSequence(wordPair);
		}
	}

	public void putCategory(Category category)
	{
		ContentValues values = new ContentValues();
		values.put(OpenHelper.FIELDNAME_ID, category.getID());
		values.put(OpenHelper.FIELDNAME_NAME, category.getName());
		values.put(OpenHelper.FIELDNAME_IMAGENAME, category.getImageName());
		values.put(OpenHelper.FIELDNAME_USAGE, category.getUsageCount());
		mDatabase.replace(OpenHelper.TABLE_CATEGORY, null, values);
	}

	public void putSentence(Sentence sentence)
	{
		ContentValues values = new ContentValues();
		values.put(OpenHelper.FIELDNAME_ID, sentence.getID());
		values.put(OpenHelper.FIELDNAME_CATEGORYID, sentence.getCategoryID());
		values.put(OpenHelper.FIELDNAME_CONTENT, sentence.getContent());
		values.put(OpenHelper.FIELDNAME_USAGE, sentence.getUsageCount());
		mDatabase.replace(OpenHelper.TABLE_SENTENCE, null, values);
	}

	public void putWordPairSequence(WordPairSequence wordPair)
	{
		ContentValues values = new ContentValues();
		//values.put(OpenHelper.FIELDNAME_ID, wordPair.getID());
		values.put(OpenHelper.FIELDNAME_THISWORD, wordPair.getThisWord());
		values.put(OpenHelper.FIELDNAME_NEXTWORD, wordPair.getNextWord());
		values.put(OpenHelper.FIELDNAME_COUNTER, wordPair.getCount());
		mDatabase.replace(OpenHelper.TABLE_WORDSEQUENCE, null, values);
	}

	public long getNextCategoryId()
	{
		String sql = String.format("SELECT MAX(%s) FROM %s", OpenHelper.FIELDNAME_ID, OpenHelper.TABLE_CATEGORY);
		Cursor cursor = mDatabase.rawQuery(sql, null);
		if (cursor.moveToFirst())
		{
			return cursor.getLong(0) + 1;
		}
		else
		{
			return 1L;
		}
	}

	public long getNextSentenceId()
	{
		String sql = String.format("SELECT MAX(%s) FROM %s", OpenHelper.FIELDNAME_ID, OpenHelper.TABLE_SENTENCE);
		Cursor cursor = mDatabase.rawQuery(sql, null);
		if (cursor.moveToFirst())
		{
			return cursor.getLong(0) + 1;
		}
		else
		{
			return 1L;
		}
	}

	public void increaseCategoryUsage(long category_id)
	{
		mOpenHelper.increment(OpenHelper.TABLE_CATEGORY, OpenHelper.FIELDNAME_USAGE, OpenHelper.FIELDNAME_ID, String.valueOf(category_id));
	}

	public void increaseSentenceUsage(long sentence_id)
	{
		mOpenHelper.increment(OpenHelper.TABLE_SENTENCE, OpenHelper.FIELDNAME_USAGE, OpenHelper.FIELDNAME_ID, String.valueOf(sentence_id));
	}

	public void updateWordPairs(String text)
	{
		String[] words = text.split(" ");

		String thisWord = "";
		for (String nextWord : words)
		{
			String columns [] = { OpenHelper.FIELDNAME_ID };
			String selection = OpenHelper.FIELDNAME_THISWORD + " = ? AND " + OpenHelper.FIELDNAME_NEXTWORD + " = ?";
			String selectionArgs [] = { Utils.filter(thisWord), Utils.filter(nextWord) };
			Cursor cursor = mDatabase.query(OpenHelper.TABLE_WORDSEQUENCE, columns, selection, selectionArgs, null, null, null);
			if (cursor.moveToFirst())
			{
				mOpenHelper.increment(OpenHelper.TABLE_WORDSEQUENCE, OpenHelper.FIELDNAME_COUNTER, OpenHelper.FIELDNAME_ID, cursor.getString(0));
			}
			else
			{
				putWordPairSequence(new WordPairSequence(0, thisWord, nextWord, 1));
			}

			if (nextWord.endsWith(".") || nextWord.endsWith("!") || nextWord.endsWith("?"))
			{
				thisWord = "";
			}
			else
			{
				thisWord = nextWord;
			}
		}
	}

	public void deleteCategories()
	{
		mDatabase.delete(OpenHelper.TABLE_CATEGORY, null, null);
	}

	public void deleteSentences()
	{
		mDatabase.delete(OpenHelper.TABLE_SENTENCE, null, null);
	}

	public void deleteWordPairSequences()
	{
		mDatabase.delete(OpenHelper.TABLE_WORDSEQUENCE, null, null);
	}

	public void deleteCategory(long category_id)
	{
		String whereClause = OpenHelper.FIELDNAME_ID + " = ?";
		String whereArgs [] = { String.valueOf(category_id) };
		mDatabase.delete(OpenHelper.TABLE_CATEGORY, whereClause, whereArgs);
	}

	public void deleteSentence(long sentence_id)
	{
		String whereClause = OpenHelper.FIELDNAME_ID + " = ?";
		String whereArgs [] = { String.valueOf(sentence_id) };
		mDatabase.delete(OpenHelper.TABLE_SENTENCE, whereClause, whereArgs);
	}

	public void deleteWordPairSequence(long id)
	{
		String whereClause = OpenHelper.FIELDNAME_ID + " = ?";
		String whereArgs [] = { String.valueOf(id) };
		mDatabase.delete(OpenHelper.TABLE_WORDSEQUENCE, whereClause, whereArgs);
	}

	public void deleteEverything()
	{
		deleteCategories();
		deleteSentences();
		deleteWordPairSequences();
	}
}
