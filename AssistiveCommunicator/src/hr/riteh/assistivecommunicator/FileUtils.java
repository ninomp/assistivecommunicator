package hr.riteh.assistivecommunicator;

import java.io.File;
import java.util.List;

public abstract class FileUtils
{
	public static void findFilesRecursively(List<File> foundFiles /* out */, File directory /* in */, String nameEndsWith /* in */)
	{
		File files [] = directory.listFiles();
		if (files != null)
		{
			for (File file : files)
			{
				if (file.isFile() && file.getName().endsWith(nameEndsWith))
				{
					foundFiles.add(file);
				}
				else if (file.isDirectory())
				{
					findFilesRecursively(foundFiles, file, nameEndsWith);
				}
			}
		}
	}

	public static void deleteFilesInDirectory(File directory)
	{
		for (File file : directory.listFiles())
		{
			file.delete();
		}
	}
}
