package hr.riteh.assistivecommunicator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import hr.riteh.assistivecommunicator.dialogs.ShowTextDialog;

public class CategorySentencesActivity extends Activity implements AdapterView.OnItemClickListener
{
	// Key names for extra intent data
	private static final String EXTRA_CATEGORY = "category_id";

	public static void start(Context context, long category_id)
	{
		Intent intent = new Intent(context, CategorySentencesActivity.class);
		intent.putExtra(EXTRA_CATEGORY, category_id);
		context.startActivity(intent);
	}

	private static final String STATE_EDITMODE = "editmode";

	private long mCategoryID;
	private Settings mSettings;
	private Database mDatabase;
	private SentencesAdapter mAdapter;
	private ShowTextDialog mShowTextDialog;
	private Speech mSpeech;
	private TextView mEditModeIndicator;
	private boolean mEditMode = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mCategoryID = getIntent().getLongExtra(EXTRA_CATEGORY, 0);

		mSettings = new Settings(this);

		mDatabase = new Database(this);

		mAdapter = new SentencesAdapter(mDatabase.getCategorySentences(mCategoryID, mSettings.getSentencesOrder()));

		mShowTextDialog = new ShowTextDialog(this);

		mSpeech = new Speech(this);

		setContentView(R.layout.activity_sentences);

		ListView listview = (ListView) findViewById(R.id.listview);
		listview.setAdapter(mAdapter);
		listview.setOnItemClickListener(this);

		mEditModeIndicator = (TextView) findViewById(R.id.editmode_indicator);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		// Load and apply orientation mode
		setRequestedOrientation(mSettings.getOrientationMode());

		// Reload data from database
		mAdapter.setData(mDatabase.getCategorySentences(mCategoryID, mSettings.getSentencesOrder()));
	}

	@Override
	protected void onDestroy()
	{
		mSpeech.close();

		mDatabase.close();

		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		// Save activity state
		outState.putBoolean(STATE_EDITMODE, mEditMode);

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		// Always call the superclass so it can restore the view hierarchy state
		super.onRestoreInstanceState(savedInstanceState);

		// Restore state of activity from saved instance
		mEditMode = savedInstanceState.getBoolean(STATE_EDITMODE);
		mEditModeIndicator.setVisibility(mEditMode ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (mEditMode)
		{
			EditSentenceActivity.startEdit(this, id);
		}
		else
		{
			Sentence sentence = mAdapter.getItem(position);

			mDatabase.increaseSentenceUsage(id);

			if (mSettings.getShowText())
			{
				mShowTextDialog.setMessage(sentence.getContent());
				mShowTextDialog.show();
			}

			if (mSettings.getSpeakText())
			{
				mSpeech.speak(sentence.getContent());
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_categorysentences, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.menuitem_editmode:
				mEditMode = !mEditMode;
				mEditModeIndicator.setVisibility(mEditMode ? View.VISIBLE : View.GONE);
				return true;
			case R.id.menuitem_create:
				EditSentenceActivity.startCreate(this, mCategoryID);
				return true;
			case R.id.menuitem_entertext:
				startActivity(new Intent(this, EnterTextActivity.class));
				return true;
			case R.id.menuitem_settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void onBack(View view)
	{
		finish();
	}
}
