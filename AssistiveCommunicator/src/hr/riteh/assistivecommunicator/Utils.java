package hr.riteh.assistivecommunicator;

import java.util.Locale;

public abstract class Utils
{
	public static int limit(int value, int min, int max)
	{
		if (value < min)
		{
			return min;
		}
		else if (value > max)
		{
			return max;
		}
		else
		{
			return value;
		}
	}

	public static String filter(String s)
	{
		s = s.toLowerCase(Locale.getDefault());
		s = s.replace('ć', 'c');
		s = s.replace('č', 'c');
		s = s.replace('š', 's');
		s = s.replace('ž', 'z');
		s = s.replace('đ', 'd');
		return s.replaceAll("[^a-z0-9]", "");
	}

	public static int hash(String s)
	{
		return filter(s).hashCode();
	}
}
