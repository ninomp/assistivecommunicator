package hr.riteh.assistivecommunicator;

import java.io.File;
import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FileListAdapter extends BaseAdapter
{
	private List<File> mFiles;

	public FileListAdapter(List<File> files)
	{
		mFiles = files;
	}

	@Override
	public int getCount()
	{
		return mFiles.size();
	}

	@Override
	public File getItem(int position)
	{
		return mFiles.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView textview = (TextView) convertView;

		if (textview == null)
		{
			textview = new TextView(parent.getContext());
		}

		textview.setText(getItem(position).getName());

		return textview;
	}
}
