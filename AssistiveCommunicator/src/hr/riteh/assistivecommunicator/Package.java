package hr.riteh.assistivecommunicator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public abstract class Package
{
	public static String FILE_EXTENSION = ".dmk";

	private static String ENTRYNAME_CATEGORIES = "categories.csv";
	private static String ENTRYNAME_SENTENCES  = "sentences.csv";
	private static String ENTRYNAME_WORDPAIRS  = "wordpairs.csv";

	public static void doImport(File packageFile, Database database, FileResourceManager fileResourceManager) throws IOException
	{
		// Open package file
		ZipFile zipFile = new ZipFile(packageFile);

		// Load categories from package
		List<Category> categories = deserializeCategories(loadCSVEntryFromZipFile(zipFile, ENTRYNAME_CATEGORIES));

		// Load sentences from package
		List<Sentence> sentences = deserializeSentences(loadCSVEntryFromZipFile(zipFile, ENTRYNAME_SENTENCES));

		// Load wordPairs from package
		List<WordPairSequence> wordPairs = deserializeWordPairs(loadCSVEntryFromZipFile(zipFile, ENTRYNAME_WORDPAIRS));

		// Extract images
		Set<String> images = getCategoryImages(categories);
		for (String imageName : images)
		{
			File imageFile = fileResourceManager.getImageFile(imageName);
			extractEntryFromZipFile(zipFile, imageName, imageFile);
		}

		// Store Category objects to database
		database.putCategories(categories);

		// Store Sentence objects to database
		database.putSentences(sentences);

		// Store WordPairSequence objects to database
		database.putWordPairSequences(wordPairs);

		// Close package file
		zipFile.close();
	}

	public static void doExport(File packageFile, Database database, FileResourceManager fileResourceManager) throws IOException
	{
		FileOutputStream fos = new FileOutputStream(packageFile);
		ZipOutputStream zos = new ZipOutputStream(fos);

		List<Category> categories = database.getCategories(null);
		zos.putNextEntry(new ZipEntry(ENTRYNAME_CATEGORIES));
		zos.write(serializeCategories(categories).getBytes());
		zos.closeEntry();

		List<Sentence> sentences = database.getSentences(null);
		zos.putNextEntry(new ZipEntry(ENTRYNAME_SENTENCES));
		zos.write(serializeSentences(sentences).getBytes());
		zos.closeEntry();

		List<WordPairSequence> wordPairs = database.getWordPairSequences(null);
		zos.putNextEntry(new ZipEntry(ENTRYNAME_WORDPAIRS));
		zos.write(serializeWordPairs(wordPairs).getBytes());
		zos.closeEntry();

		Set<String> images = getCategoryImages(categories);
		for (String imageName : images)
		{
			File imageFile = fileResourceManager.getImageFile(imageName);
			addEntryToZipOutputStream(zos, imageFile.getName(), imageFile);
		}

		zos.close();
		fos.close();
	}

	private static String serializeCategories(List<Category> categories)
	{
		CSV.Writer writer = new CSV.Writer();

		writer.write("ID", "Name", "ImageName", "UsageCount");

		for (Category category : categories)
		{
			writer.write(category.getID(), category.getName(), category.getImageName(), category.getUsageCount());
		}

		return writer.toString();
	}

	private static List<Category> deserializeCategories(List<Map<String, String>> serializedCategories) throws IOException
	{
		List<Category> categories = new ArrayList<Category>();

		for (Map<String, String> serializedCategory : serializedCategories)
		{
			long id = Long.parseLong(serializedCategory.get("ID"));
			String name = serializedCategory.get("Name");
			String image = serializedCategory.get("ImageName");
			String usage = serializedCategory.get("UsageCount");
			int usageCount = usage != null ? Integer.parseInt(usage) : 0;

			categories.add(new Category(id, name, image, usageCount));
		}

		return categories;
	}

	private static String serializeSentences(List<Sentence> sentences)
	{
		CSV.Writer writer = new CSV.Writer();

		writer.write("ID", "CategoryID", "Content", "UsageCount");

		for (Sentence sentence : sentences)
		{
			writer.write(sentence.getID(), sentence.getCategoryID(), sentence.getContent(), sentence.getUsageCount());
		}

		return writer.toString();
	}

	private static List<Sentence> deserializeSentences(List<Map<String, String>> serializedSentences)
	{
		List<Sentence> sentences = new ArrayList<Sentence>();

		for (Map<String, String> serializedSentence : serializedSentences)
		{
			long id = Long.parseLong(serializedSentence.get("ID"));
			long category_id = Long.parseLong(serializedSentence.get("CategoryID"));
			String content = serializedSentence.get("Content");
			String usage = serializedSentence.get("UsageCount");
			int usage_count = usage != null ? Integer.parseInt(usage) : 0;

			sentences.add(new Sentence(id, category_id, content, usage_count));
		}

		return sentences;
	}

	private static String serializeWordPairs(List<WordPairSequence> wordPairs)
	{
		CSV.Writer writer = new CSV.Writer();

		writer.write("This", "Next", "Count");

		for (WordPairSequence wordPair : wordPairs)
		{
			writer.write(wordPair.getThisWord(), wordPair.getNextWord(), wordPair.getCount());
		}

		return writer.toString();
	}

	private static List<WordPairSequence> deserializeWordPairs(List<Map<String, String>> serializedWordPairs)
	{
		List<WordPairSequence> wordPairs = new ArrayList<WordPairSequence>();

		for (Map<String, String> serializedWordPair : serializedWordPairs)
		{
			String thisWord = serializedWordPair.get("This");
			String nextWord = serializedWordPair.get("Next");
			int count = Integer.parseInt(serializedWordPair.get("Count"));

			wordPairs.add(new WordPairSequence(0L, thisWord, nextWord, count));
		}

		return wordPairs;
	}

	private static Set<String> getCategoryImages(List<Category> categories)
	{
		Set<String> images = new HashSet<String>();

		for (Category category : categories)
		{
			images.add(category.getImageName());
		}

		return images;
	}

	private static List<Map<String, String>> loadCSVEntryFromZipFile(ZipFile zipFile, String entryName) throws IOException
	{
		return CSV.parseStream(zipFile.getInputStream(zipFile.getEntry(entryName)), "UTF-8");
	}

	private static void extractEntryFromZipFile(ZipFile zipFile, String entryName, File destinationFile) throws IOException
	{
		// Prepare streams
		InputStream inputStream = zipFile.getInputStream(zipFile.getEntry(entryName));
		OutputStream outputStream = new FileOutputStream(destinationFile);

		// Copy data
		copyStream(inputStream, outputStream);

		// Close streams
		outputStream.close();
		inputStream.close();
	}

	private static void addEntryToZipOutputStream(ZipOutputStream zipOutputStream, String entryName, File sourceFile) throws IOException
	{
		// Prepare streams
		FileInputStream fileInputStream = new FileInputStream(sourceFile);
		zipOutputStream.putNextEntry(new ZipEntry(entryName));

		// Copy data
		copyStream(fileInputStream, zipOutputStream);

		// Close streams
		zipOutputStream.closeEntry();
		fileInputStream.close();
	}

	private static void copyStream(InputStream inputStream, OutputStream outputStream) throws IOException
	{
		byte[] buffer = new byte[1024];
		int bytes_read;
		while ((bytes_read = inputStream.read(buffer)) > 0)
		{
			outputStream.write(buffer, 0, bytes_read);
		}
	}
}
