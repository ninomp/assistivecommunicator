package hr.riteh.assistivecommunicator;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;

public class SentencesAdapter extends BaseListAdapter<Sentence>
{
	public SentencesAdapter(List<Sentence> list)
	{
		super(list);
	}

	@Override
	public long getItemID(Sentence sentence)
	{
		return sentence.getID();
	}

	@Override
	public View createView(ViewGroup parent)
	{
		return new SentenceView(parent.getContext());
	}

	@Override
	public void bindView(View view, Sentence sentence)
	{
		SentenceView sentenceView = (SentenceView) view;
		sentenceView.setSentence(sentence);
	}
}
