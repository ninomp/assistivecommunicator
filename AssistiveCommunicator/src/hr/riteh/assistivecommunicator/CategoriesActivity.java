package hr.riteh.assistivecommunicator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

public class CategoriesActivity extends Activity implements AdapterView.OnItemClickListener
{
	private static final String STATE_EDITMODE = "editmode";

	private Settings mSettings;
	private Database mDatabase;
	private CategoriesAdapter mAdapter;
	private PagingGridView mGridView;
	private TextView mEditModeIndicator;
	private boolean mEditMode = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mSettings = new Settings(this);

		mDatabase = new Database(this);

		mAdapter = new CategoriesAdapter(mDatabase.getCategories(mSettings.getCategoriesOrder()));

		setContentView(R.layout.activity_categories);

		mGridView = (PagingGridView) findViewById(R.id.gridview);
		mGridView.setAdapter(mAdapter);
		mGridView.setOnItemClickListener(this);

		mEditModeIndicator = (TextView) findViewById(R.id.editmode_indicator);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		// Load and apply orientation mode
		setRequestedOrientation(mSettings.getOrientationMode());

		// Load page size from preferences
		mGridView.setPageSize(mSettings.getCategoriesPageSize());

		// Reload data from database
		mAdapter.setData(mDatabase.getCategories(mSettings.getCategoriesOrder()));
	}

	@Override
	protected void onDestroy()
	{
		mDatabase.close();

		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		// Save activity state
		outState.putBoolean(STATE_EDITMODE, mEditMode);

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		// Always call the superclass so it can restore the view hierarchy state
		super.onRestoreInstanceState(savedInstanceState);

		// Restore state of activity from saved instance
		mEditMode = savedInstanceState.getBoolean(STATE_EDITMODE);
		mEditModeIndicator.setVisibility(mEditMode ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (mEditMode)
		{
			EditCategoryActivity.start(this, id);
		}
		else
		{
			mDatabase.increaseCategoryUsage(id);

			CategorySentencesActivity.start(this, id);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_categories, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.menuitem_editmode:
				mEditMode = !mEditMode;
				mEditModeIndicator.setVisibility(mEditMode ? View.VISIBLE : View.GONE);
				return true;
			case R.id.menuitem_create:
				startActivity(new Intent(this, EditCategoryActivity.class));
				return true;
			case R.id.menuitem_import:
				startActivity(new Intent(this, ImportActivity.class));
				return true;
			case R.id.menuitem_export:
				startActivity(new Intent(this, ExportActivity.class));
				return true;
			case R.id.menuitem_entertext:
				startActivity(new Intent(this, EnterTextActivity.class));
				return true;
			case R.id.menuitem_settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void onEnterText(View view)
	{
		startActivity(new Intent(this, EnterTextActivity.class));
	}

	public void onPrevPage(View view)
	{
		mGridView.prevPage();
	}

	public void onNextPage(View view)
	{
		mGridView.nextPage();
	}

	public void onPrevLine(View view)
	{
		mGridView.prevLine();
	}

	public void onNextLine(View view)
	{
		mGridView.nextLine();
	}
}
