package hr.riteh.assistivecommunicator;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

public class KeyboardlessEditText extends EditText
{
	public KeyboardlessEditText(Context context)
	{
		super(context);
	}

	public KeyboardlessEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public KeyboardlessEditText(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int inputType = getInputType();     // Backup the input type
		setInputType(InputType.TYPE_NULL);  // Disable standard keyboard
		super.onTouchEvent(event);          // Call handler from superclass
		setInputType(inputType);            // Restore input type
		return true;                        // Consume touch event
	}
}
