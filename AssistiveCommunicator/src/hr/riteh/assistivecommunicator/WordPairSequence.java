package hr.riteh.assistivecommunicator;

public class WordPairSequence
{
	private long   mID;
	private String mThisWord;
	private String mNextWord;
	private int    mCount;

	public WordPairSequence(long id, String thisWord, String nextWord, int count)
	{
		mID       = id;
		mThisWord = Utils.filter(thisWord);
		mNextWord = Utils.filter(nextWord);
		mCount    = count;
	}

	public long getID()
	{
		return mID;
	}

	public void setID(long id)
	{
		mID = id;
	}

	public String getThisWord()
	{
		return mThisWord;
	}

	public void setThisWord(String thisWord)
	{
		mThisWord = Utils.filter(thisWord);
	}

	public String getNextWord()
	{
		return mNextWord;
	}

	public void setNextWord(String nextWord)
	{
		mNextWord = Utils.filter(nextWord);
	}

	public int getCount()
	{
		return mCount;
	}

	public void setCount(int count)
	{
		mCount = count;
	}
}
