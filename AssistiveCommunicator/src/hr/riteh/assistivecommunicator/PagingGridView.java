package hr.riteh.assistivecommunicator;

import java.util.LinkedList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;

public class PagingGridView extends AdapterView<ListAdapter>
{
	private static final int INVALID_INDEX = -1;

	private boolean mOptimize = false;
	private int mPageSize;

	private int mColumns;
	private int mRows;

	private int mOffset = 0;

	private int mSelectedIndex = -1;

	private ListAdapter mAdapter = null;

	private LinkedList<View> mChildViewRecycler = new LinkedList<View>();

	public PagingGridView(Context context)
	{
		super(context);
	}

	public PagingGridView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public PagingGridView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public int getColumnCount()
	{
		return mColumns;
	}

	public void setColumnCount(int count)
	{
		mOptimize = false;
		mColumns = count;

		requestLayout();
	}

	public int getRowCount()
	{
		return mRows;
	}

	public void setRowCount(int count)
	{
		mOptimize = false;
		mRows = count;

		requestLayout();
	}

	public int getPageSize()
	{
		return mColumns * mRows;
	}

	public void setPageSize(int size)
	{
		mOptimize = true;
		mPageSize = size;

		requestLayout();
	}

	public int getOffset()
	{
		return mOffset;
	}

	public void setOffset(int offset)
	{
		int page_size = mColumns * mRows;
		int count = mAdapter.getCount();

		if (count > page_size)
		{
			mOffset = Utils.limit(offset, 0, count - page_size);
		}
		else
		{
			mOffset = 0;
		}

		requestLayout();

		invalidate();
	}

	public void prevLine()
	{
		setOffset(mOffset - mColumns);
	}

	public void nextLine()
	{
		setOffset(mOffset + mColumns);
	}

	public void prevPage()
	{
		setOffset(mOffset - mColumns * mRows);
	}

	public void nextPage()
	{
		setOffset(mOffset + mColumns * mRows);
	}

	@Override
	public ListAdapter getAdapter()
	{
		return mAdapter;
	}

	@Override
	public void setAdapter(ListAdapter adapter)
	{
		mAdapter = adapter;

		requestLayout();
	}

	@Override
	public int getSelectedItemPosition()
	{
		return mSelectedIndex;
	}

	@Override
	public long getSelectedItemId()
	{
		return mAdapter.getItemId(mSelectedIndex + mOffset);
	}

	@Override
	public View getSelectedView()
	{
		return getChildAt(mSelectedIndex);
	}

	@Override
	public void setSelection(int position)
	{
		// Remove selection from previously selected
		updateChildSelection(mSelectedIndex, false);

		// Update internal selected index
		mSelectedIndex = position;

		// Put selection to newly selected
		updateChildSelection(mSelectedIndex, true);
	}

	private void updateChildSelection(int position, boolean selected)
	{
		View child = getChildAt(position);
		if (child != null)
		{
			child.setSelected(selected);
			child.setBackgroundColor(selected ? Color.LTGRAY : Color.BLACK);
		}
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		super.onLayout(changed, left, top, right, bottom);

		if (mAdapter == null)
		{
			removeAllViewsInLayout();
			return;
		}

		recycleChildren();

		detachAllViewsFromParent();

		int width = right - left;
		int height = bottom - top;

		if (mOptimize)  optimizeGeometry(mPageSize, width, height);

		int childWidth = width / mColumns;
		int childHeight = height / mRows;

		int visibleChildren = Math.min(mAdapter.getCount() - mOffset, mColumns * mRows);

		for (int index = 0; index < visibleChildren; index++)
		{
			View child = mAdapter.getView(index + mOffset, mChildViewRecycler.poll(), this);
			addChild(child, index, childWidth, childHeight);
		}
	}

	private void optimizeGeometry(int page_size, int width, int height)
	{
		int min_diff = Integer.MAX_VALUE;
		int min_cols = 0;
		int min_rows = 0;

		for (int i = 1; i <= page_size; i++)
		{
			if (page_size % i == 0)
			{
				int cols = i;
				int rows = page_size / i;

				int item_width = getWidth() / cols;
				int item_height = getHeight() / rows;

				int diff = Math.abs(item_width - item_height);

				if (diff < min_diff)
				{
					min_diff = diff;
					min_cols = cols;
					min_rows = rows;
				}
			}
		}

		if (min_cols > 0 && min_rows > 0)
		{
			mColumns = min_cols;
			mRows = min_rows;
		}
		else
		{
			mColumns = 1;
			mRows = 1;
		}
	}

	private void recycleChildren()
	{
		for (int i = 0; i < getChildCount(); i++)
		{
			mChildViewRecycler.add(getChildAt(i));
		}
	}

	private void addChild(View child, int index, int childWidth, int childHeight)
	{
		LayoutParams params = child.getLayoutParams();
		if (params == null)
		{
			params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		}

		addViewInLayout(child, index, params, true);

		child.measure(MeasureSpec.EXACTLY | childWidth, MeasureSpec.EXACTLY | childHeight);

		int col = index % mColumns;
		int row = index / mColumns;

		child.layout(col * childWidth, row * childHeight, (col + 1) * childWidth, (row + 1) * childHeight);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		switch (event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				setSelection(pointToPosition((int) event.getX(), (int) event.getY()));
				break;

			case MotionEvent.ACTION_UP:
				clickChildAt((int) event.getX(), (int) event.getY());
				break;
		}

		return true;
	}

	private void clickChildAt(int x, int y)
	{
		int index = pointToPosition(x, y);
		if (index != INVALID_INDEX)
		{
			View child = getChildAt(index);
			int position = index + mOffset;
			long id = mAdapter.getItemId(position);
			performItemClick(child, position, id);
		}
	}

	private int pointToPosition(int x, int y)
	{
		Rect rect = new Rect();
		for (int i = 0; i < getChildCount(); i++)
		{
			getChildAt(i).getHitRect(rect);
			if (rect.contains(x, y))
			{
				return i;
			}
		}

		return INVALID_INDEX;
	}
}
