package hr.riteh.assistivecommunicator;

public class Sentence
{
	private long   mID;
	private long   mCategoryID;
	private String mContent;
	private int    mUsageCount;

	public Sentence()
	{
	}

	public Sentence(long id, long category_id, String content, int usageCount)
	{
		mID = id;
		mCategoryID = category_id;
		mContent = content;
		mUsageCount = usageCount;
	}

	public long getID()
	{
		return mID;
	}

	public void setID(long id)
	{
		mID = id;
	}

	public long getCategoryID()
	{
		return mCategoryID;
	}

	public void setCategoryID(long category_id)
	{
		mCategoryID = category_id;
	}

	public String getContent()
	{
		return mContent;
	}

	public void setContent(String content)
	{
		mContent = content;
	}

	public int getUsageCount()
	{
		return mUsageCount;
	}

	public void setUsageCount(int usageCount)
	{
		mUsageCount = usageCount;
	}

	@Override
	public int hashCode()
	{
		return Utils.hash(mContent);
	}
}
