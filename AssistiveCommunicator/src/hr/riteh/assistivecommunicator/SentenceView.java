package hr.riteh.assistivecommunicator;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class SentenceView extends TextView
{
	private Settings mSettings;

	public SentenceView(Context context)
	{
		super(context);
		init(context);
	}

	public SentenceView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public SentenceView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context)
	{
		mSettings = new Settings(context);
	}

	public void setSentence(Sentence sentence)
	{
		setTextSize(mSettings.getListTextSize());

		if (mSettings.getShowUsage())
		{
			setText(String.format("%s (%d)", sentence.getContent(), sentence.getUsageCount()));
		}
		else
		{
			setText(sentence.getContent());
		}
	}
}
