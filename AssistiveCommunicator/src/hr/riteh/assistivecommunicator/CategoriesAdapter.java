package hr.riteh.assistivecommunicator;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;

public class CategoriesAdapter extends BaseListAdapter<Category>
{
	public CategoriesAdapter(List<Category> list)
	{
		super(list);
	}

	@Override
	public long getItemID(Category category)
	{
		return category.getID();
	}

	@Override
	public View createView(ViewGroup parent)
	{
		return new CategoryView(parent.getContext());
	}

	@Override
	public void bindView(View view, Category category)
	{
		CategoryView categoryview = (CategoryView) view;
		categoryview.setCategory(category);
	}
}
