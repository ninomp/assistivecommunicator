package hr.riteh.assistivecommunicator;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import hr.riteh.assistivecommunicator.dialogs.ConfirmDialog;
import hr.riteh.assistivecommunicator.dialogs.SimpleMessageDialog;

public class EditSentenceActivity extends Activity implements ConfirmDialog.OnConfirmListener
{
	// Key names for extra intent data
	private static final String EXTRA_CATEGORY = "category_id";
	private static final String EXTRA_SENTENCE = "sentence_id";

	public static void startCreate(Context context, long category_id)
	{
		Intent intent = new Intent(context, EditSentenceActivity.class);
		intent.putExtra(EXTRA_CATEGORY, category_id);
		context.startActivity(intent);
	}

	public static void startEdit(Context context, long sentence_id)
	{
		Intent intent = new Intent(context, EditSentenceActivity.class);
		intent.putExtra(EXTRA_SENTENCE, sentence_id);
		context.startActivity(intent);
	}

	private long mSentenceID;
	private Database mDatabase;
	private Sentence mSentence;
	private ArrayAdapter<Category> mAdapterCategory;
	private Spinner mSpinnerCategory;
	private EditText mEditTextContent;

	private SimpleMessageDialog mEmptyContentMessageDialog;
	private ConfirmDialog mConfirmDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mSentenceID = getIntent().getLongExtra(EXTRA_SENTENCE, 0);

		mDatabase = new Database(this);

		mSentence = mDatabase.getSentence(mSentenceID);

		if (mSentence == null)
		{
			mSentence = new Sentence();
			mSentence.setID(mDatabase.getNextSentenceId());
			mSentence.setCategoryID(getIntent().getLongExtra(EXTRA_CATEGORY, 0));
		}

		setContentView(R.layout.activity_editsentence);

		mAdapterCategory = new ArrayAdapter<Category>(this, android.R.layout.simple_spinner_item, mDatabase.getCategories(null));
		mAdapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
		mSpinnerCategory.setAdapter(mAdapterCategory);

		for (int position = 0; position < mAdapterCategory.getCount(); position++)
		{
			if (mAdapterCategory.getItem(position).getID() == mSentence.getCategoryID())
			{
				mSpinnerCategory.setSelection(position);
			}
		}

		mEditTextContent = (EditText) findViewById(R.id.editTextContent);
		mEditTextContent.setText(mSentence.getContent());

		mEmptyContentMessageDialog = new SimpleMessageDialog(this);
		mEmptyContentMessageDialog.setTitle(R.string.emptysentence_title);
		mEmptyContentMessageDialog.setMessage(R.string.emptysentence_text);

		mConfirmDialog = new ConfirmDialog(this);
		mConfirmDialog.setOnConfirmListener(this);
		mConfirmDialog.setTitle(R.string.confirmdelete_title);
		mConfirmDialog.setMessage(R.string.confirmdelete_text);
	}

	@Override
	protected void onDestroy()
	{
		mDatabase.close();

		super.onDestroy();
	}

	public void onSaveClicked(View view)
	{
		if (mEditTextContent.length() == 0)
		{
			mEmptyContentMessageDialog.show();

			return;
		}

		Category category = (Category) mSpinnerCategory.getSelectedItem();
		mSentence.setCategoryID(category.getID());
		mSentence.setContent(mEditTextContent.getText().toString());
		mDatabase.putSentence(mSentence);

		finish();
	}

	public void onCancelClicked(View view)
	{
		finish();
	}

	public void onDeleteClicked(View view)
	{
		mConfirmDialog.show();
	}

	@Override
	public void onConfirm(DialogInterface dialog)
	{
		mDatabase.deleteSentence(mSentenceID);

		finish();
	}
}
