package hr.riteh.assistivecommunicator;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import hr.riteh.assistivecommunicator.dialogs.ConfirmDialog;

public class ExportActivity extends Activity implements ConfirmDialog.OnConfirmListener
{
	private Settings mSettings;

	private EditText mEditTextFilename;

	private File mPackageFile;

	private ConfirmDialog mConfirmDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mSettings = new Settings(this);

		setRequestedOrientation(mSettings.getOrientationMode());

		setContentView(R.layout.activity_export);

		mEditTextFilename = (EditText)findViewById(R.id.editText_filename);
		mEditTextFilename.setText(DateFormat.format("yyyyMMdd_kkmmss", System.currentTimeMillis()) + Package.FILE_EXTENSION);
		mEditTextFilename.selectAll();

		mConfirmDialog = new ConfirmDialog(this);
		mConfirmDialog.setOnConfirmListener(this);
		mConfirmDialog.setTitle(R.string.confirmoverwrite_title);
		mConfirmDialog.setMessage(R.string.confirmoverwrite_text);
	}

	public void onExportClicked(View view)
	{
		String filename = mEditTextFilename.getText().toString().trim();

		if (!filename.endsWith(Package.FILE_EXTENSION))
		{
			filename += Package.FILE_EXTENSION;
		}

		mPackageFile = new File(Environment.getExternalStorageDirectory(), filename);

		if (mPackageFile.exists())
		{
			mConfirmDialog.show();
		}
		else
		{
			doExport();
		}
	}

	@Override
	public void onConfirm(DialogInterface dialog)
	{
		doExport();
	}

	private void doExport()
	{
		Database database = new Database(this);
		FileResourceManager fileResourceManager = new FileResourceManager(this);

		try
		{
			Package.doExport(mPackageFile, database, fileResourceManager);

			Toast.makeText(this, R.string.export_success, Toast.LENGTH_SHORT).show();
		}
		catch (IOException e)
		{
			e.printStackTrace();

			Toast.makeText(this, R.string.export_fail, Toast.LENGTH_SHORT).show();
		}

		database.close();
	}

	public void onDoneClicked(View view)
	{
		finish();
	}
}
