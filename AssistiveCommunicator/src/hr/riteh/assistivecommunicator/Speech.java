package hr.riteh.assistivecommunicator;

import android.content.Context;
import android.speech.tts.TextToSpeech;

public class Speech implements TextToSpeech.OnInitListener
{
	private TextToSpeech mTextToSpeech;

	public Speech(Context context)
	{
		mTextToSpeech = new TextToSpeech(context, this);
	}

	@Override
	public void onInit(int status)
	{
	}

	public void speak(String text)
	{
		mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
	}

	public void close()
	{
		mTextToSpeech.stop();
		mTextToSpeech.shutdown();
	}
}
