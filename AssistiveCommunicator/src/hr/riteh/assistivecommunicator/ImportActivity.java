package hr.riteh.assistivecommunicator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import hr.riteh.assistivecommunicator.dialogs.ConfirmDialog;
import hr.riteh.assistivecommunicator.dialogs.ListChooserDialog;

public class ImportActivity extends Activity implements ListChooserDialog.OnChosenListener, ConfirmDialog.OnConfirmListener
{
	private Settings mSettings;

	private TextView mTextViewChosenPackageFullPath;
	private CheckBox mCheckBoxDeleteBeforeImport;
	private Button mButtonImport;

	private List<File> mFoundPackages = new ArrayList<File>();
	private FileListAdapter mAdapter = new FileListAdapter(mFoundPackages);
	private ListChooserDialog mPackageChooserDialog;
	private File mChosenPackage;

	private ConfirmDialog mConfirmDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// Create an instance of settings helper
		mSettings = new Settings(this);

		// Apply orientation mode preference option
		setRequestedOrientation(mSettings.getOrientationMode());

		// Load layout
		setContentView(R.layout.activity_import);

		// Retrieve references to layout views
		mTextViewChosenPackageFullPath = (TextView) findViewById(R.id.textView_chosenfilepath);
		mCheckBoxDeleteBeforeImport = (CheckBox) findViewById(R.id.checkbox_deletebeforeimport);
		mButtonImport = (Button) findViewById(R.id.button_import);

		// Scan external storage for package files
		FileUtils.findFilesRecursively(mFoundPackages, Environment.getExternalStorageDirectory(), Package.FILE_EXTENSION);

		// Create dialog for choosing package file
		mPackageChooserDialog = new ListChooserDialog(this);
		mPackageChooserDialog.setOnChosenListener(this);
		mPackageChooserDialog.setAdapter(mAdapter);
		mPackageChooserDialog.setTitle(R.string.choosepackage_title);

		// Create dialog for confirming deletion
		mConfirmDialog = new ConfirmDialog(this);
		mConfirmDialog.setOnConfirmListener(this);
		mConfirmDialog.setTitle(R.string.confirmdeletebeforeimport_title);
		mConfirmDialog.setMessage(R.string.confirmdeletebeforeimport_text);
	}

	public void onChooseFileClicked(View view)
	{
		// Show dialog for choosing package to import
		mPackageChooserDialog.show();
	}

	@Override
	public void onChosen(int position)
	{
		// Get reference to selected file
		mChosenPackage = mFoundPackages.get(position);

		// Display full file path on TextView
		mTextViewChosenPackageFullPath.setText(mChosenPackage.getAbsolutePath());

		// Enable 'Import' button
		mButtonImport.setEnabled(true);
	}

	public void onImportClicked(View view)
	{
		if (mCheckBoxDeleteBeforeImport.isChecked())
		{
			mConfirmDialog.show();
		}
		else
		{
			doImport(false);
		}
	}

	@Override
	public void onConfirm(DialogInterface dialog)
	{
		doImport(true);
	}

	private void doImport(boolean clear)
	{
		// Prepare
		Database database = new Database(this, true);
		FileResourceManager fileResourceManager = new FileResourceManager(this);

		try
		{
			if (clear)
			{
				// Delete all records from database
				database.deleteEverything();

				// Delete all image files
				FileUtils.deleteFilesInDirectory(fileResourceManager.getImagesDir());
			}

			// Actually do import
			Package.doImport(mChosenPackage, database, fileResourceManager);

			Toast.makeText(this, R.string.import_success, Toast.LENGTH_SHORT).show();
		}
		catch (Exception e)
		{
			e.printStackTrace();

			Toast.makeText(this, R.string.import_fail, Toast.LENGTH_SHORT).show();
		}

		database.close();
	}

	public void onDoneClicked(View view)
	{
		// Close (this) activity
		finish();
	}
}
