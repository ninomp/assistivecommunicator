package hr.riteh.assistivecommunicator;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import hr.riteh.assistivecommunicator.dialogs.ConfirmDialog;
import hr.riteh.assistivecommunicator.dialogs.SimpleMessageDialog;

public class EditCategoryActivity extends Activity implements ConfirmDialog.OnConfirmListener
{
	// Key names for extra intent data
	private static final String EXTRA_CATEGORY = "category_id";

	// Request codes for starting activities for result
	private static final int REQUEST_CODE_PICK_IMAGE = 1;

	public static void start(Context context, long category_id)
	{
		Intent intent = new Intent(context, EditCategoryActivity.class);
		intent.putExtra(EXTRA_CATEGORY, category_id);
		context.startActivity(intent);
	}

	private long mCategoryID;
	private Database mDatabase;
	private Category mCategory;
	private EditText mEditTextName;
	private ImageView mImageViewImage;
	private FileResourceManager mFileResourceManager;

	private SimpleMessageDialog mEmptyNameMessageDialog;
	private ConfirmDialog mConfirmDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mCategoryID = getIntent().getLongExtra(EXTRA_CATEGORY, 0);

		mFileResourceManager = new FileResourceManager(this);

		mDatabase = new Database(this);

		mCategory = mDatabase.getCategory(mCategoryID);

		if (mCategory == null)
		{
			mCategory = new Category();
			mCategory.setID(mDatabase.getNextCategoryId());
			mCategory.setImageName("");
		}

		setContentView(R.layout.activity_editcategory);

		mEditTextName = (EditText) findViewById(R.id.editTextName);
		mEditTextName.setText(mCategory.getName());

		mImageViewImage = (ImageView) findViewById(R.id.imageViewImage);
		setImageViewImage(mCategory.getImageName());

		mEmptyNameMessageDialog = new SimpleMessageDialog(this);
		mEmptyNameMessageDialog.setTitle(R.string.emptycategoryname_title);
		mEmptyNameMessageDialog.setMessage(R.string.emptycategoryname_text);

		mConfirmDialog = new ConfirmDialog(this);
		mConfirmDialog.setOnConfirmListener(this);
		mConfirmDialog.setTitle(R.string.confirmdelete_title);
		mConfirmDialog.setMessage(R.string.confirmdelete_text);
	}

	@Override
	protected void onDestroy()
	{
		mDatabase.close();

		super.onDestroy();
	}

	public void onChooseImageClicked(View view)
	{
		Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
		getIntent.setType("image/*");

		Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		pickIntent.setType("image/*");

		Intent chooserIntent = Intent.createChooser(getIntent, getText(R.string.choosecategoryimage));
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { pickIntent });

		startActivityForResult(chooserIntent, REQUEST_CODE_PICK_IMAGE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == RESULT_OK)
		{
			String image = FileResourceManager.getFullPathFromUri(getBaseContext(), data.getData());

			setImageViewImage(image);

			mCategory.setImageName(image);
		}
	}

	public void onSaveClicked(View view)
	{
		if (mEditTextName.length() == 0)
		{
			mEmptyNameMessageDialog.show();

			return;
		}

		mCategory.setName(mEditTextName.getText().toString());
		mDatabase.putCategory(mCategory);

		finish();
	}

	public void onCancelClicked(View view)
	{
		finish();
	}

	public void onDeleteClicked(View view)
	{
		mConfirmDialog.show();
	}

	@Override
	public void onConfirm(DialogInterface dialog)
	{
		mDatabase.deleteCategory(mCategoryID);

		finish();
	}

	private void setImageViewImage(String image)
	{
		Bitmap bitmap = BitmapFactory.decodeFile(mFileResourceManager.getImagePath(image));
		if (bitmap != null)
		{
			mImageViewImage.setImageBitmap(bitmap);
		}
		else
		{
			mImageViewImage.setImageResource(R.drawable.category_default);
		}
	}
}
