package hr.riteh.assistivecommunicator;

public class Category
{
	private long   mID;
	private String mName;
	private String mImageName;
	private int    mUsageCount;

	public Category()
	{
	}

	public Category(String name)
	{
		mName = name;
	}

	public Category(long id, String name, String imageName, int usageCount)
	{
		mID = id;
		mName = name;
		mImageName = imageName;
		mUsageCount = usageCount;
	}

	public long getID()
	{
		return mID;
	}

	public void setID(long id)
	{
		mID = id;
	}

	public String getName()
	{
		return mName;
	}

	public void setName(String name)
	{
		mName = name;
	}

	public String getImageName()
	{
		return mImageName;
	}

	public void setImageName(String imageName)
	{
		mImageName = imageName;
	}

	public int getUsageCount()
	{
		return mUsageCount;
	}

	public void setUsageCount(int usageCount)
	{
		mUsageCount = usageCount;
	}

	@Override
	public String toString()
	{
		return mName;
	}

	@Override
	public int hashCode()
	{
		return Utils.hash(mName);
	}
}
