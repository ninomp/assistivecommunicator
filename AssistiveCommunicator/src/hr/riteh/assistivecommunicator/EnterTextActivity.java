package hr.riteh.assistivecommunicator;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;

import hr.riteh.assistivecommunicator.dialogs.ShowTextDialog;

public class EnterTextActivity extends Activity implements KeyboardView.OnKeyboardActionListener, TextWatcher, OnItemClickListener
{
	private Settings mSettings;

	private Database mDatabase;

	private KeyboardView mKeyboardView;

	private EditText mEditText;

	private GridView mGridViewSuggestions;

	private ShowTextDialog mShowTextDialog;

	private Speech mSpeech;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mSettings = new Settings(this);

		setRequestedOrientation(mSettings.getOrientationMode());

		mDatabase = new Database(this);

		mShowTextDialog = new ShowTextDialog(this);

		mSpeech = new Speech(this);

		setContentView(R.layout.activity_entertext);

		mKeyboardView = (KeyboardView) findViewById(R.id.keyboardView);
		mKeyboardView.setOnKeyboardActionListener(this);

		mEditText = (EditText) findViewById(R.id.editText);
		mEditText.addTextChangedListener(this);

		mGridViewSuggestions = (GridView) findViewById(R.id.gridviewSuggestions);
		mGridViewSuggestions.setOnItemClickListener(this);

		showSuggestions("", "");
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		mKeyboardView.setKeyboard(new Keyboard(this, mSettings.getKeyboardType()));
	}

	@Override
	protected void onDestroy()
	{
		mSpeech.close();

		super.onDestroy();
	}

	public void onClearClicked(View view)
	{
		mEditText.setText("");

		showSuggestions("", "");
	}

	public void onShowClicked(View view)
	{
		String text = mEditText.getText().toString();

		mDatabase.updateWordPairs(text);

		if (mSettings.getShowText())
		{
			mShowTextDialog.setMessage(text);
			mShowTextDialog.show();
		}

		if (mSettings.getSpeakText())
		{
			mSpeech.speak(text);
		}
	}

	@Override
	public void onKey(int primaryCode, int[] keyCodes)
	{
		// Create and dispatch 'key down' event
		dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, primaryCode));
	}

	@Override
	public void onPress(int primaryCode)
	{
	}

	@Override
	public void onRelease(int primaryCode)
	{
	}

	@Override
	public void onText(CharSequence text)
	{
	}

	@Override
	public void swipeDown()
	{
	}

	@Override
	public void swipeLeft()
	{
	}

	@Override
	public void swipeRight()
	{
	}

	@Override
	public void swipeUp()
	{
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_entertext, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.menuitem_settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private String mSuggestionThisWord = null;
	private List<String> mAllSuggestions = null;

	private void showSuggestions(String thisWord, String nextWordBeginning)
	{
		if (mSuggestionThisWord == null || !mSuggestionThisWord.equalsIgnoreCase(thisWord))
		{
			mAllSuggestions = mDatabase.getWordSuggestions(Utils.filter(thisWord));
			mSuggestionThisWord = thisWord;
		}

		int max_suggestions = mSettings.getWordSuggestion();

		List<String> suggestions = new ArrayList<String>();

		for (String word : mAllSuggestions)
		{
			if (suggestions.size() >= max_suggestions)
			{
				break;
			}

			if (word.startsWith(nextWordBeginning))
			{
				suggestions.add(word);
			}
		}

		mGridViewSuggestions.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_text_item, suggestions));
	}

	@Override
	public void afterTextChanged(Editable s)
	{
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		String text = s.toString();

		String[] words = text.split(" ");

		String thisWord = "";
		String nextWord = "";

		int size = words.length;

		if (text.endsWith(" "))
		{
			if (size > 0)
			{
				thisWord = words[size - 1];
			}
		}
		else
		{
			if (size > 1)
			{
				thisWord = words[size - 2];
				nextWord = words[size - 1];
			}
			else if (size > 0)
			{
				nextWord = words[size - 1];
			}
		}

		showSuggestions(thisWord, nextWord);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		String text = mEditText.getText().toString();

		String newWord = (String) parent.getItemAtPosition(position);

		// Last index of space in sentence
		if (!text.endsWith(" "))
		{
			int space_pos = text.lastIndexOf(" ");

			// If there is space in sentence get last word
			if (space_pos > 0)
			{
				text = text.substring(0, space_pos);
				text += " ";
			}
			// Else empty text - to replace first part of text
			else
			{
				text = "";
			}
		}
		else
		{
			text = text.trim();
			text += " ";
		}

		mEditText.setText(String.format("%s%s ", text, newWord));
		mEditText.setSelection(mEditText.getText().length());
	}
}
