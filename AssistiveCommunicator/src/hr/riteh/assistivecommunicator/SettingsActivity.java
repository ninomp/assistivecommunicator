package hr.riteh.assistivecommunicator;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener
{
	private Settings mSettings;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mSettings = new Settings(this);

		addPreferencesFromResource(R.xml.preferences);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		setRequestedOrientation(mSettings.getOrientationMode());

		mSettings.registerOnChangeListener(this);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		mSettings.unregisterOnChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		if (Settings.SETTING_ORIENTATIONMODE.equals(key))
		{
			setRequestedOrientation(mSettings.getOrientationMode());
		}
	}
}
