package hr.riteh.assistivecommunicator;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CategoryView extends LinearLayout
{
	private FileResourceManager mFileResourceManager;
	private Settings mSettings;

	private ImageView mImageView;
	private TextView  mTextView;

	public CategoryView(Context context)
	{
		super(context);
		init(context);
	}

	public CategoryView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	private void init(Context context)
	{
		// Create an instance of file resource manager
		mFileResourceManager = new FileResourceManager(context);

		// Create an instance of preferences access helper
		mSettings = new Settings(context);

		// Make this vertical linear layout
		setOrientation(LinearLayout.VERTICAL);

		// Center (child) views
		setGravity(Gravity.CENTER_HORIZONTAL);

		// Create (child) views and add them to layout
		mImageView = new ImageView(context);
		mTextView  = new TextView (context);
		addView(mImageView, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 0, 1.0f));
		addView(mTextView, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}

	public void setCategory(Category category)
	{
		String imageName = category.getImageName();
		if (imageName.length() > 0)
		{
			String filename = mFileResourceManager.getImagePath(imageName);
			mImageView.setImageBitmap(BitmapFactory.decodeFile(filename));
		}
		else
		{
			mImageView.setImageResource(R.drawable.category_default);
		}

		if (mSettings.getShowUsage())
		{
			mTextView.setText(String.format("%s (%d)", category.getName(), category.getUsageCount()));
		}
		else
		{
			mTextView.setText(category.getName());
		}
	}
}
